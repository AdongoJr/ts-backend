import express, { Request, Response } from "express";

const app: express.Application = express();
const PORT: number = 3000;

app.get("/", (req: Request, res: Response) => {
  res.status(200).json({ message: "Welcome to my API" });
});

app.listen(PORT, () => {
  return console.log(`Server listening on port: ${PORT}...`);
});

